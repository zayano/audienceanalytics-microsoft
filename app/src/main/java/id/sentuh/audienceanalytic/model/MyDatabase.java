package id.sentuh.audienceanalytic.model;


import com.dbflow5.annotation.ConflictAction;
import com.dbflow5.annotation.Database;

/**
 * Created by user on 29/4/19.
 */
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION, insertConflict = ConflictAction.IGNORE, updateConflict= ConflictAction.REPLACE)
public class MyDatabase {
    public static final String NAME = "ConsoleAnalytic"; // we will add the .db extension

    public static final int VERSION = 1;

}
