package id.sentuh.audienceanalytic.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wajah {
    @SerializedName("face_id")
    @Expose
    private String faceId;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("emotion")
    @Expose
    private String emotion;
    public Wajah(){

    }
    public Wajah(String faceId, Integer age, String gender, String emotion) {
        this.faceId = faceId;
        this.age = age;
        this.gender = gender;
        this.emotion = emotion;
    }

    public String getFaceId() {
        return faceId;
    }

    public void setFaceId(String faceId) {
        this.faceId = faceId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    @Override
    public String toString() {
        return "Wajah{" +
                "faceId='" + faceId + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", emotion='" + emotion + '\'' +
                '}';
    }
}
