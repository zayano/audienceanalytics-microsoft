package id.sentuh.audienceanalytic;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FaceRectangle;
import com.microsoft.projectoxford.face.contract.VerifyResult;
import com.microsoft.projectoxford.face.rest.ClientException;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraView;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Permission;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import id.sentuh.audienceanalytic.app.API;
import id.sentuh.audienceanalytic.app.AppConfig;
import id.sentuh.audienceanalytic.app.AppUtils;
import id.sentuh.audienceanalytic.app.GPSTracker;
import id.sentuh.audienceanalytic.model.Wajah;
import id.sentuh.audienceanalytic.result.TokenResult;

public class MainActivity extends AppCompatActivity {
    private static final String TAG="Analytic";
    CameraView cameraView;
    Handler mHandler;
//    VisionServiceClient mClient;
    FaceServiceClient faceClient;
    TextView textView;
    LinearLayout mContainer;
    private final String apiEndpoint = "https://australiaeast.api.cognitive.microsoft.com/face/v1.0";
    private final String subscriptionKey = "55f452abcf584dbb9b5255f81b0b2c0c";

    AppConfig config;
    String accessToken;
    Date dateExpired;
    GPSTracker tracker;
    int intTimeExpired;
    ProgressBar progressBar;
    String deviceId;
    String ipLocal;
    String serialNumber;
    List<Wajah> mWajahs;
    List<Bitmap> bmpFaces;
    List<ScanResult> wifiList;
    WifiManager wifi;
    WifiInfo wifiInfo;
    Switch swCamera;
    Switch swMode;
    boolean modeTrain =false;
    UUID lastFaceUUId=UUID.randomUUID();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }
        deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        config = new AppConfig(this);

        serialNumber = Build.SERIAL;
        accessToken = config.getAccessToken();
        intTimeExpired = config.getTokenExpired();
        cameraView = findViewById(R.id.camera);
        textView = (TextView) findViewById(R.id.info);
        mContainer = (LinearLayout) findViewById(R.id.img_container);
        progressBar = (ProgressBar) findViewById(R.id.loading_progress);
        swCamera = findViewById(R.id.switch_camera);
        swMode = findViewById(R.id.switch_mode);


        swCamera.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(cameraView.isFacingBack()){
                    cameraView.setFacing(CameraKit.Constants.FACING_FRONT);
                } else {
                    cameraView.setFacing(CameraKit.Constants.FACING_BACK);
                }
            }
        });
        swMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                modeTrain = isChecked;
            }
        });
//        if (mClient==null){
//            mClient = new VisionServiceRestClient(getString(R.string.subscription_key), getString(R.string.subscription_apiroot));
//        }

        if(faceClient==null){
            faceClient = new FaceServiceRestClient(apiEndpoint, subscriptionKey);
        }
        showProgress(false);
        cameraView.setFacing(CameraKit.Constants.FACING_FRONT);
        mHandler = new Handler();
        //getToken();
        //dateExpired = new Date(intTimeExpired);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(intTimeExpired);
        dateExpired = cal.getTime();
        if(accessToken.equals("")){
            getToken();
        }
        Log.d(TAG,String.format("token %s expired : %s %d",accessToken,dateExpired.toString(),intTimeExpired));

        //Searching device location by GPS
        tracker =new GPSTracker(this);
        if (tracker.canGetLocation()){
            double lat = tracker.getLatitude();
            double lng = tracker.getLongitude();
            String fcm_token = config.getFCMToken();
            int projectId = config.getPageId();
            DecimalFormat decimalFormat = new DecimalFormat("#,###.#####");
            Log.d(TAG,"Device Location: "+decimalFormat.format(lat)+", "+decimalFormat.format(lng));
            if (AppUtils.isInternetAvailable(this)){
//                AppUtils.registerDevice(MainActivity.this,deviceId,ipLocal,1,serialNumber,lat,lng,accessToken,fcm_token,projectId);
            }
        } else {
//            Log.d(TAG, "Cannot Get Location");
//            Toast.makeText(MainActivity.this,"Cannot Get Location",Toast.LENGTH_SHORT).show();
        }


    }

    //Request Camera Permission
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    //Request Permission
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.LOCATION_HARDWARE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION

        }, 101);
    }

    //permission device
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==101){
            if (grantResults[0]== PackageManager.PERMISSION_GRANTED){
                mHandler.postDelayed(mGetCaptureImage,500);
            }
        }
    }
    void showProgress(boolean value){
        if(value){
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
    private Runnable mGetCaptureImage = new Runnable() {
        @Override
        public void run() {
            cameraView.captureImage(new CameraKitEventCallback<CameraKitImage>() {
                @Override
                public void callback(CameraKitImage cameraKitImage) {
                Log.d(TAG,"start capture");
                    new AnalizeFaceBitmap().execute(cameraKitImage.getBitmap());
                }
            });

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
        mHandler.postDelayed(mGetCaptureImage,1500);

    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacks(mGetCaptureImage);
        super.onDestroy();
    }

    //get token for login
    private void getToken(){
//        LoginResult result = new LoginResult();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("grant_type","password");
        params.put("username","admin@sentuhdigital.com");
        params.put("password","sembarang");
        params.put("client_id",2);
        params.put("client_secret","fD2cLYUqigNcQVGAsE7GKFjyOR0WMu8QJy47M1ZJ");
        params.put("scope","*");

        client.post(API.GET_TOKEN,params,new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String jsonstring = new String(responseBody);
                Log.d(TAG,"json token:"+jsonstring);
                try {
                    Gson gson = new GsonBuilder().create();
                    TokenResult tokenResult = gson.fromJson(jsonstring,TokenResult.class);
                    accessToken = tokenResult.getAccessToken();
                    config.setAccessToken(tokenResult.getAccessToken());
                    config.setTokenExpired(tokenResult.getExpiresIn());
                    config.setTokenType(tokenResult.getTokenType());
                } catch (Exception ex){
                    Log.e(TAG,ex.getMessage());
                    Toast.makeText(MainActivity.this,"Parsing Gagal!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
//                showProgress(true);
            }

            @Override
            public void onFinish() {
                super.onFinish();
//                showProgress(false);
                mHandler.postDelayed(mGetCaptureImage,300);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(MainActivity.this,"Ambil token Gagal!",Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Store Audience to API Console
    private void storeAudience(List<Wajah> wajahs){
//        LoginResult result = new LoginResult();
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization","Bearer "+accessToken);
        Gson gson =new GsonBuilder().create();
        RequestParams params = new RequestParams();
        params.put("device_id",deviceId);
        for(Wajah item:wajahs){
            String json = gson.toJson(item,Wajah.class);
            Log.d(TAG, "Array String face: "+json);
            params.add("face[]",json);
        }

        client.post(API.STORE_AUDIENCE,params,new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String jsonstring = new String(responseBody);
                Log.d(TAG,"json audience:"+jsonstring);
                try {
                    Gson gson = new GsonBuilder().create();
                    JSONObject json = new JSONObject(jsonstring);
                    boolean status = json.getBoolean("status");
                    String message = json.getString("message");
                    if(status){
                        Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex){
                    Log.e(TAG,ex.getMessage());
                    Toast.makeText(MainActivity.this,"Parsing Gagal!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
//                showProgress(true);
            }

            @Override
            public void onFinish() {
                super.onFinish();
//                showProgress(false);
                mHandler.postDelayed(mGetCaptureImage,100);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String jsonstring = new String(responseBody);
                Log.e(TAG,"json audience error:"+jsonstring);
                Toast.makeText(MainActivity.this,"Posting wajah Gagal!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Detect Face audience by capture for jpeg
    private void detectAndFrame(final Bitmap imageBitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        ByteArrayInputStream inputStream =
                new ByteArrayInputStream(outputStream.toByteArray());

        AnalizeFaceBitmap detectTask = new AnalizeFaceBitmap();
       // detectTask.execute(inputStream);
    }

    //Face Detected Frame
    private static Bitmap drawFaceRectanglesOnBitmap(
            Bitmap originalBitmap, Face[] faces) {
        Bitmap bitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(10);
        if (faces != null) {
            for (Face face : faces) {
                FaceRectangle faceRectangle = face.faceRectangle;
                canvas.drawRect(
                        faceRectangle.left,
                        faceRectangle.top,
                        faceRectangle.left + faceRectangle.width,
                        faceRectangle.top + faceRectangle.height,
                        paint);
            }
        }
        return bitmap;
    }

    //Analize face to Bitmap
    private class AnalizeFaceBitmap extends AsyncTask<Bitmap, String, Face[]>{
//        InputStream mInputStream;
        Bitmap mBitmap;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgress(true);
        }

        @Override
        protected Face[] doInBackground(Bitmap... params) {
            try {
//                mInputStream = params[0];
//                mBitmap= BitmapFactory.decodeStream(mInputStream);
                mBitmap = params[0];
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
                ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());

                //Face Parameter
                Face[] result = faceClient.detect(
                        inputStream,
                        true,         // returnFaceId
                        false,        // returnFaceLandmarks
                                new FaceServiceClient.FaceAttributeType[] {
                                    FaceServiceClient.FaceAttributeType.Age,
                                    FaceServiceClient.FaceAttributeType.Gender,
                                        FaceServiceClient.FaceAttributeType.Emotion,
                                }
                );
                return result;
            } catch (Exception ex){
                return null;
            }
        }

        //Face Execution
        @Override
        protected void onPostExecute(Face[] faces) {
//            super.onPostExecute(faces);
            if(mContainer.getChildCount()>1){
                mContainer.removeAllViews();
            }
            if(textView==null){
                textView = new TextView(MainActivity.this);
                textView.setTextColor(Color.WHITE);
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                mContainer.addView(textView);
            }

            mWajahs = new ArrayList<Wajah>();
            bmpFaces = new ArrayList<Bitmap>();
            if(faces!=null && faces.length>0){
//                StringBuilder sb = new StringBuilder();

                for(Face face:faces){
                    int left = face.faceRectangle.left;
                    int top = face.faceRectangle.top;
                    int width = face.faceRectangle.width;
                    int height = face.faceRectangle.height;

                    //Emotion Prediction
                    Bitmap face_bmp = Bitmap.createBitmap(mBitmap,left,top,width,height);
                    ImageView image = new ImageView(MainActivity.this);
                    image.setLayoutParams(new LinearLayout.LayoutParams(96,96));
                    image.setScaleType(ImageView.ScaleType.FIT_XY);
                    image.setImageBitmap(face_bmp);
                    mContainer.addView(image);
                    String gender=face.faceAttributes.gender;
                    String emotion = "Biasa";
                    if(face.faceAttributes.emotion.anger>0.7f){
                        emotion="Marah";
                    }
                    if(face.faceAttributes.emotion.contempt>0.7f){
                        emotion="Mencibir";
                    }
                    if(face.faceAttributes.emotion.fear>0.7f){
                        emotion="Takut";
                    }
                    if(face.faceAttributes.emotion.happiness>0.7f){
                        emotion="Bahagia";
                    }
                    if(face.faceAttributes.emotion.sadness>0.7f){
                        emotion="Sedih";
                    }
                    if(face.faceAttributes.emotion.surprise>0.7f){
                        emotion="Terkejut";
                    }
                    if(face.faceAttributes.emotion.neutral>0.7f){
                        emotion="Biasa";
                    }
                    textView.setText(face.faceId.toString());
//                    sb.append(String.format("Age : %03f, \nGender : %s \r\nExpresi : %s",
//                            face.faceAttributes.age,gender,emotion));
                    Wajah wajah=new Wajah(face.faceId.toString(),(int)face.faceAttributes.age,gender,emotion);
//                    if(!face.faceId.equals(lastFaceUUId)){
                        mWajahs.add(wajah);
//                        bmpFaces.add(face_bmp);
//                        if(modeTrain){
//                            try {
//                                VerifyResult verifyResult = faceClient.verify(face.faceId,lastFaceUUId);
//                                String groupName = lastFaceUUId.toString().replace("-","");
//                                if(!verifyResult.isIdentical){
////                                    storeSingleFace(face_bmp,wajah);
//                                        Log.d(TAG,String.format("face %s %s not identical",lastFaceUUId.toString(),face.faceId.toString()));
////                                    faceClient.createPersonGroup(groupName,"","");
//                                } else {
////                                    faceClient.trainPersonGroup(groupName);
//                                    Log.d(TAG,String.format("face %s %s identical",lastFaceUUId.toString(),face.faceId.toString()));
//                                }
//
//                            } catch (ClientException e) {
//                                Toast.makeText(MainActivity.this,e.error.message,Toast.LENGTH_SHORT).show();
//                                Log.d(TAG,"error face : "+e.getMessage());
//                                e.printStackTrace();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//                    }
                    lastFaceUUId = face.faceId;

                }
                storeAudience(mWajahs);
                if(!modeTrain){
//                    storeFaces(bmpFaces,mWajahs);

                }

            } else {
                textView.setText("No Face..");

            }
            mHandler.postDelayed(mGetCaptureImage,1500);
        }
    }
//    private class AnalizeVisionBitmap extends AsyncTask<Bitmap,Void,String>{
//        private Bitmap mBitmap;
//        @Override
//        protected String doInBackground(Bitmap... bitmaps) {
//            try {
//                mBitmap = bitmaps[0];
//                Gson gson = new Gson();
//                String[] features = {"ImageType", "Color", "Faces", "Adult", "Categories"};
//                String[] details = {};
//
//                // Put the image into an input stream for detection.
//                ByteArrayOutputStream output = new ByteArrayOutputStream();
//                mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
//                ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
//
//                AnalysisResult v = mClient.analyzeImage(inputStream, features, details);
//
//                String result = gson.toJson(v);
//                Log.d("result", "result : "+result);
//                return result;
//            } catch (Exception ex){
//
//            }
//            return null;
//        }
//        @Override
//        protected void onPostExecute(String data) {
//            Gson gson = new Gson();
//            AnalysisResult result = gson.fromJson(data, AnalysisResult.class);
//            int fcount=result.faces.size();
//            //result.description

//        }
//
//    }

    //Create Image file JPG format
    private File createImageFile(String imageFileName) throws IOException {

        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    //Store Face Detection to API Console
    private void storeFaces(List<Bitmap> bmps,List<Wajah> faces){
    AsyncHttpClient client = new AsyncHttpClient();
    client.addHeader("Authorization","Bearer "+accessToken);
    client.addHeader("Accept","multipart/form-data");
    //client.addHeader("Content-Type","multipart/form-data");
    RequestParams params = new RequestParams();
    try {
        Gson gson =new GsonBuilder().create();
        final File[] files = new File[faces.size()];
        int i=0;
        for(Wajah item:faces){

            String json = gson.toJson(item,Wajah.class);
            Log.d(TAG,"Parameter StoreFaces: "+json);
            params.add("face[]",json);
            files[i]=createImageFile(item.getFaceId());
            i++;
        }
        i=0;
        for(Bitmap bmp:bmps){
//            ByteArrayOutputStream output = new ByteArrayOutputStream();
            FileOutputStream fos = new FileOutputStream(files[i]);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            params.put("bitmap[]",files[i],"application/octet-stream");
            i++;
        }

        String urlUpdate = API.STORE_FACE;
        Log.d(TAG,"url store face :"+urlUpdate);
        client.post(urlUpdate, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String json_string = new String(responseBody);
                Log.d(TAG,"json upload face :"+json_string);
                try {
                    JSONObject json = new JSONObject(json_string);
                    boolean status = json.getBoolean("status");
                    String message = json.getString("message");
                    if(status){
                        Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                    }
                    for(int i=0;i<files.length;i++){
                        files[i].delete();
                    }
                    mHandler.postDelayed(mGetCaptureImage,1500);
                } catch (Exception ex){
                    Log.e(TAG,"error sending face :"+ex.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String json_string = new String(responseBody);
                Log.e(TAG,"error url image update :"+json_string);
                mHandler.postDelayed(mGetCaptureImage,500);
            }
        });
        } catch (Exception ex){
            Log.e(TAG,"error store face :"+ex.getMessage());
        }
    }

    //Store Single Face Audience to API Console
    private void storeSingleFace(Bitmap bmp,Wajah face){
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization","Bearer "+accessToken);
        client.addHeader("Accept","multipart/form-data");
        //client.addHeader("Content-Type","multipart/form-data");
        RequestParams params = new RequestParams();
        try {
            Gson gson =new GsonBuilder().create();
            String json = gson.toJson(face,Wajah.class);
            params.add("face",json);
            final File file=createImageFile(face.getFaceId());
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            params.put("bitmap",file,"application/octet-stream");

            String urlUpdate = API.STORE_SINGLE_FACE;
            Log.d(TAG,"url store face :"+urlUpdate);
            client.post(urlUpdate, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String json_string = new String(responseBody);
                    Log.d(TAG,"json upload face :"+json_string);
                    try {
                        JSONObject json = new JSONObject(json_string);
                        boolean status = json.getBoolean("status");
                        String message = json.getString("message");
                        if(status){
                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        }
                        file.delete();
                        mHandler.postDelayed(mGetCaptureImage,1500);
                    } catch (Exception ex){
                        Log.e(TAG,"error sending face :"+ex.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    String json_string = new String(responseBody);
                    Log.e(TAG,"error url image update :"+json_string);
                    mHandler.postDelayed(mGetCaptureImage,500);
                }
            });
        } catch (Exception ex){
            Log.e(TAG,"error store face :"+ex.getMessage());
        }
    }
}
