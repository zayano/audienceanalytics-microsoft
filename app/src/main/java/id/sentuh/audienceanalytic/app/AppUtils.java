package id.sentuh.audienceanalytic.app;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public abstract class AppUtils {
    private static final String TAG="Utils";

    //Checking Internet Available
    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean NisConnected = activeNetwork != null && activeNetwork.isConnected();
        if (NisConnected) {
            //  if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE || activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else return activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
        }
        return false;
    }

    //Register Device to API console
    public static void registerDevice(final Context context, String deviceId, String ipLocal, Integer productId,
                                      String serialNumber, Double lat, Double lng,
                                      String token, String fcm_token, Integer projectId){

        try {
//        AsyncHttpClient client = new AsyncHttpClient();
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization","Bearer "+token);
//        client.addHeader("Accept","application/octet-stram");
//        client.addHeader("Content-Type","application/json");
        RequestParams params = new RequestParams();
        params.put("device_id",deviceId);
        params.put("ip_local",ipLocal);
        params.put("product_id",productId);
        params.put("serial_number",serialNumber);
        params.put("latitude",lat);
        params.put("longitude",lng);
        params.put("fem_token",fcm_token);
        params.put("project_id",projectId);
        Log.d(TAG,"Device ID : "+deviceId);

        String UrlRegister = API.DEVICE_REGISTER;
        Log.d(TAG,"REGISTER URL : "+ UrlRegister +"\n"+params.toString());

        client.post(UrlRegister, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String json_string = new String(responseBody);
                Log.d(TAG,"json register device : "+json_string);
                try {
                    JSONObject json = new JSONObject(json_string);
                    boolean status = json.getBoolean("status");
                    String message = json.getString("message");
                    Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                    if(status) {
                        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception ex){
                        Toast.makeText(context,ex.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String json_string =new String(responseBody);
                Log.e(TAG,"error register device : "+json_string);
//                Gson gson = new GsonBuilder().create();
//                ErrorResult errorResult = gson.fromJson(json_string,ErrorResult.class);
//                Toast.makeText(context,errorResult.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

        } catch (Exception ex){
            Log.e(TAG, "error register device : "+ex.getMessage());
        }

    }
}
