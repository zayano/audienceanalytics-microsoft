package id.sentuh.audienceanalytic.app;

import android.app.Application;
import android.content.Intent;

import com.dbflow5.config.FlowManager;

import id.sentuh.audienceanalytic.MyService;

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }
}
