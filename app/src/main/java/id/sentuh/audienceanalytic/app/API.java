package id.sentuh.audienceanalytic.app;

public abstract class API {
    public static final String TAG = "Audience Analytic";
    public static final String BASE_SERVER = "http://console.sentuh.id";
    public static final String API_SERVER = BASE_SERVER +"/api";
    public static final String LOGIN = API_SERVER + "/login";
    public static final String GET_TOKEN = BASE_SERVER + "/oauth/token";
    public static final String DEVICE_REGISTER = API_SERVER + "/device/register"; //POST or STORE
    public static final String STORE_AUDIENCE = API_SERVER + "/audience"; //POST or STORE
    public static final String STORE_FACE = API_SERVER + "/face"; //POST or STORE
    public static final String STORE_SINGLE_FACE = API_SERVER + "/face/single"; //POST or STORE
    public static final String GET_AUDIENCE = API_SERVER + "/audience"; //POST or STORE
    public static final String GET_FACE = API_SERVER + "/face"; //POST or STORE
//    public static final String CREATE_FACE_LIST="https://[location].api.cognitive.microsoft.com/face/v1.0/facelists/{faceListId}";
    //update foto /users/update/profile/{id}
    public static final String UPDATE_PROFILE_PICTURE = API_SERVER + "/users/update/profile/%d";
}
